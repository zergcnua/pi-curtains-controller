package com.ua.cn.zerg.rpi.curtains.configuration;

import com.ua.cn.zerg.rpi.curtains.property.RemoteControlProperties;
import com.ua.cn.zerg.rpi.curtains.service.curtain.CurtainService;
import com.ua.cn.zerg.rpi.curtains.service.curtain.DefaultCurtainService;
import com.ua.cn.zerg.rpi.curtains.service.remotecontrol.DefaultRemoteControlService;
import com.ua.cn.zerg.rpi.curtains.service.remotecontrol.RemoteControlService;
import com.ua.cn.zerg.rpi.curtains.service.gpio.GpioService;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(RemoteControlProperties.class)
public class CurtainConfiguration {

    @Bean
    RemoteControlService remoteControlService(GpioService gpioService, RemoteControlProperties properties) {
        return new DefaultRemoteControlService(gpioService, properties);
    }

    @Bean
    CurtainService curtainService(RemoteControlService remoteControlService) {
        return new DefaultCurtainService(remoteControlService);
    }
}
