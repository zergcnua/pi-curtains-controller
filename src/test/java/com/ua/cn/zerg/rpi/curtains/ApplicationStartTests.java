package com.ua.cn.zerg.rpi.curtains;

import com.pi4j.io.gpio.GpioController;
import com.ua.cn.zerg.rpi.curtains.service.gpio.GpioService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
class ApplicationStartTests {

    @MockBean
    GpioService gpioService;

    @MockBean
    GpioController gpioController;

    @Test
    void contextLoads() {}

}
