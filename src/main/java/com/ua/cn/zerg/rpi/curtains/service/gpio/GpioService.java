package com.ua.cn.zerg.rpi.curtains.service.gpio;

import com.pi4j.io.gpio.GpioPinDigitalOutput;

public interface GpioService {

    GpioPinDigitalOutput getUpButtonPin();

    GpioPinDigitalOutput getStopButtonPin();

    GpioPinDigitalOutput getDownButtonPin();

    GpioPinDigitalOutput getLeftButtonPin();

    GpioPinDigitalOutput getRightButtonPin();

    GpioPinDigitalOutput getPowerPin();

    GpioPinDigitalOutput getWakeupPin();
}
