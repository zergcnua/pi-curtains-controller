package com.ua.cn.zerg.rpi.curtains.controller;

import com.ua.cn.zerg.rpi.curtains.service.curtain.CurtainService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("/curtains")
public class CurtainController {

    private final CurtainService service;

    @PostMapping("/{id}/open")
    public void open(@PathVariable int id) {
        service.open(id);
    }

    @PostMapping("/{id}/close")
    public void close(@PathVariable int id) {
        service.close(id);
    }

    @PostMapping("/{id}/stop")
    public void stop(@PathVariable int id) {
        service.stop(id);
    }

    @PostMapping("/{id}/favorite")
    public void favorite(@PathVariable int id) {
        service.favorite(id);
    }

}
