package com.ua.cn.zerg.rpi.curtains.service.remotecontrol;

public interface RemoteControlService {

    void pressUpButton();

    void pressDownButton();

    void pressStopButton();

    void longPressStopButton();

    void pressLeftButton();

    void pressRightButton();
}
