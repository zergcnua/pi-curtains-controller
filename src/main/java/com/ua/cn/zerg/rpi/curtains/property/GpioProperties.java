package com.ua.cn.zerg.rpi.curtains.property;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties("gpio")
public class GpioProperties {

    private int buttonUpPin;

    private int buttonStopPin;

    private int buttonDownPin;

    private int buttonLeftPin;

    private int buttonRightPin;

    private int powerPin;

}
