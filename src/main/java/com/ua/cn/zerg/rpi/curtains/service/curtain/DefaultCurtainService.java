package com.ua.cn.zerg.rpi.curtains.service.curtain;

import com.ua.cn.zerg.rpi.curtains.service.remotecontrol.RemoteControlService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import static java.lang.Math.max;
import static java.lang.Math.min;

@Slf4j
@RequiredArgsConstructor
public class DefaultCurtainService implements CurtainService {

    private static final int MAX_CURTAIN_ID = 15;

    private final RemoteControlService remoteControl;

    private int currentCurtainId = 1;

    @Override
    public synchronized void open(int curtainId) {
        log.info("Sending [OPEN] command to curtain {}", curtainId);
        selectCurtain(curtainId);
        remoteControl.pressUpButton();
        log.debug("Command [OPEN] {} done", curtainId);
    }

    @Override
    public synchronized void close(int curtainId) {
        log.info("Sending [CLOSE] command to curtain {}", curtainId);
        selectCurtain(curtainId);
        remoteControl.pressDownButton();
        log.debug("Command [CLOSE] {} done", curtainId);
    }

    @Override
    public synchronized void stop(int curtainId) {
        log.info("Sending [STOP] command to curtain {}", curtainId);
        selectCurtain(curtainId);
        remoteControl.pressStopButton();
        log.debug("Command [STOP] {} done", curtainId);
    }

    @Override
    public synchronized void favorite(int curtainId) {
        log.info("Sending [FAVORITE] command to curtain {}", curtainId);
        selectCurtain(curtainId);
        remoteControl.longPressStopButton();
        log.debug("Command [FAVORITE] {} done", curtainId);
    }

    private void selectCurtain(int targetCurtainId) {
        if (targetCurtainId == currentCurtainId) {
            log.info("Appropriate curtain {} already selected", targetCurtainId);
            return;
        }

        log.debug("Selecting curtain: {}. Starting from: {} ", targetCurtainId, currentCurtainId);
        int firstPoint = max(currentCurtainId, targetCurtainId);
        int secondPoint = min(currentCurtainId, targetCurtainId);

        int firstPath = MAX_CURTAIN_ID + 1 - firstPoint + secondPoint;
        int secondPath = firstPoint - secondPoint;

        int pathToRight;
        int pathToLeft;

        if (currentCurtainId < targetCurtainId) {
            pathToLeft = firstPath;
            pathToRight = secondPath;

        } else {
            pathToLeft = secondPath;
            pathToRight = firstPath;
        }

        if (pathToLeft < pathToRight) {
            for (int i = 0; i < pathToLeft; i++) {
                remoteControl.pressLeftButton();
            }

        } else {
            for (int i = 0; i < pathToRight; i++) {
                remoteControl.pressRightButton();
            }
        }

        currentCurtainId = targetCurtainId;
        log.info("Curtain {} selected", currentCurtainId);
    }
}
