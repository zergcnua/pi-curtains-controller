package com.ua.cn.zerg.rpi.curtains;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RPiCurtainsControllerApplication {

	public static void main(String[] args) {
		SpringApplication.run(RPiCurtainsControllerApplication.class, args);
	}

}
