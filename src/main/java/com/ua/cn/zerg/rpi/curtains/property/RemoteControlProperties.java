package com.ua.cn.zerg.rpi.curtains.property;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties("rc")
public class RemoteControlProperties {

    private int commandDuration;

    private int longCommandDuration;

    private int commandInterval;
}
