package com.ua.cn.zerg.rpi.curtains.service.gpio;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.ua.cn.zerg.rpi.curtains.property.GpioProperties;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import static com.pi4j.io.gpio.PinState.HIGH;
import static com.pi4j.io.gpio.RaspiPin.getPinByAddress;
import static java.lang.Thread.sleep;

@Slf4j
@Getter
public class DefaultGpioService implements GpioService {

    private static final int INIT_DELAY = 500;

    private final GpioPinDigitalOutput upButtonPin;
    private final GpioPinDigitalOutput stopButtonPin;
    private final GpioPinDigitalOutput downButtonPin;
    private final GpioPinDigitalOutput leftButtonPin;
    private final GpioPinDigitalOutput rightButtonPin;
    private final GpioPinDigitalOutput powerPin;

    private GpioPinDigitalOutput wakeUpPin;

    public DefaultGpioService(GpioController gpio, GpioProperties properties) {
        log.info("Initialising GPIO...\n{}", properties);
        powerPin = gpio.provisionDigitalOutputPin(getPinByAddress(properties.getPowerPin()), "Power", HIGH);
        upButtonPin = gpio.provisionDigitalOutputPin(getPinByAddress(properties.getButtonUpPin()), "Up Button", HIGH);
        stopButtonPin = gpio.provisionDigitalOutputPin(getPinByAddress(properties.getButtonStopPin()), "Stop Button", HIGH);
        downButtonPin = gpio.provisionDigitalOutputPin(getPinByAddress(properties.getButtonDownPin()), "Down Button", HIGH);
        leftButtonPin = gpio.provisionDigitalOutputPin(getPinByAddress(properties.getButtonLeftPin()), "Left Button", HIGH);
        rightButtonPin = gpio.provisionDigitalOutputPin(getPinByAddress(properties.getButtonRightPin()), "Right Button", HIGH);
        wakeUpPin = leftButtonPin;

        init();
    }

    @SneakyThrows
    private void init() {
        log.info("Resetting Remote control...");
        powerPin.toggle();
        sleep(INIT_DELAY);

        var wakeupPin = getWakeupPin();
        wakeupPin.toggle();
        sleep(INIT_DELAY);
        wakeupPin.toggle();
        sleep(INIT_DELAY);

        powerPin.toggle();
        log.info("Resetting Remote control done");
    }

    @Override
    public GpioPinDigitalOutput getWakeupPin() {
        if (wakeUpPin.equals(leftButtonPin)) {
            wakeUpPin = rightButtonPin;
            return leftButtonPin;
        }

        wakeUpPin = leftButtonPin;
        return rightButtonPin;
    }
}
