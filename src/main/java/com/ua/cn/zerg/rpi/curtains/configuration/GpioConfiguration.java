package com.ua.cn.zerg.rpi.curtains.configuration;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.ua.cn.zerg.rpi.curtains.property.GpioProperties;
import com.ua.cn.zerg.rpi.curtains.service.gpio.DefaultGpioService;
import com.ua.cn.zerg.rpi.curtains.service.gpio.GpioService;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(GpioProperties.class)
public class GpioConfiguration {

    @Bean
    GpioController gpioController() {
        return GpioFactory.getInstance();
    }

    @Bean
    GpioService gpioService(GpioController gpioController, GpioProperties gpioProperties) {
        return new DefaultGpioService(gpioController, gpioProperties);
    }
}
