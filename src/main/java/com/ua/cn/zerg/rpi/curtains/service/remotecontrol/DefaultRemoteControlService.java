package com.ua.cn.zerg.rpi.curtains.service.remotecontrol;

import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.ua.cn.zerg.rpi.curtains.property.RemoteControlProperties;
import com.ua.cn.zerg.rpi.curtains.service.gpio.GpioService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;

import static java.lang.Thread.sleep;
import static java.time.LocalDateTime.now;
import static java.time.temporal.ChronoUnit.MILLIS;

@Slf4j
@RequiredArgsConstructor
public class DefaultRemoteControlService implements RemoteControlService {

    private final GpioService gpioService;
    private final RemoteControlProperties properties;

    private LocalDateTime latestCommandTimestamp = now();

    @Override
    public synchronized void pressUpButton() {
        log.debug("Pressing UP button");
        pressRemoteControlButton(gpioService.getUpButtonPin());
    }

    @Override
    public synchronized void pressDownButton() {
        log.debug("Pressing DOWN button");
        pressRemoteControlButton(gpioService.getDownButtonPin());
    }

    @Override
    public synchronized void pressStopButton() {
        log.debug("Pressing Stop button");
        pressRemoteControlButton(gpioService.getStopButtonPin());
    }

    @Override
    public synchronized void longPressStopButton() {
        log.debug("Pressing Long Stop button");
        pressRemoteControlButton(gpioService.getStopButtonPin(), properties.getLongCommandDuration());
    }

    @Override
    public synchronized void pressLeftButton() {
        wakeupRemoteControl();
        log.debug("Pressing Left button");
        pressRemoteControlButton(gpioService.getLeftButtonPin());
    }

    @Override
    public synchronized void pressRightButton() {
        wakeupRemoteControl();
        log.debug("Pressing Right button");
        pressRemoteControlButton(gpioService.getRightButtonPin());
    }

    private void pressRemoteControlButton(GpioPinDigitalOutput buttonPin) {
        pressRemoteControlButton(buttonPin, properties.getCommandDuration());
    }

    @SneakyThrows
    private void pressRemoteControlButton(GpioPinDigitalOutput buttonPin, int commandDuration) {
        buttonPin.toggle();
        sleep(commandDuration);
        buttonPin.toggle();
        sleep(properties.getCommandInterval());

        incrementTimeStamp();
    }

    @SneakyThrows
    private void wakeupRemoteControl() {
        if (MILLIS.between(latestCommandTimestamp, now()) < 9500) {
            log.debug("Remote Control is awake");
            return;
        }
        log.debug("Wakeup Remote Control");
        pressRemoteControlButton(gpioService.getWakeupPin());
    }

    private void incrementTimeStamp() {
        latestCommandTimestamp = now();
    }
}
