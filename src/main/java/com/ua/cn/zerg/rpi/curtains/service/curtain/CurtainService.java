package com.ua.cn.zerg.rpi.curtains.service.curtain;

public interface CurtainService {

    void open(int curtainId);

    void close(int curtainId);

    void favorite(int curtainId);

    void stop(int curtainId);
}
